依赖于  ROS

# quick start

代码执行需要在 bag_parse 目录下 创建 input和 output文件夹 ，并将bag包放至 input的文件夹中

目录结构应如下所示

> .  
> ├── bag_parse.py  
> ├── input  
> ├── output  
> └── README.md

但是 bag 包 都很大 ，移动来移动去很麻烦，而且上述方法直接存储在本机中，本机内存也吃不消。

因此可以用 使用 ln -s 去创建符号连接（快捷方式）的方式 将 所有数据放在硬盘中不动。

**例如**  在/media/hao/WangHao'Disk/bag_record 中 有两个bag包

> .  
> ├── record_all_2022-04-28-16-38-55.bag  
> └── record_all_2022-04-28-16-40-33.bag

那么我在 bag_parse.py 所在目录下 打开终端 输入 

```bash
ln -s /media/hao/WangHao'Disk/bag_record $(pwd)/input

# 为了简单 我将两个目录链接到了同一个位置，其实没有什么的影响
ln -s /media/hao/WangHao'Disk/bag_record $(pwd)/output
```

通过 ls -l 命令可以看到 input和output 都是 符号链接

> ├── bag_parse.py  
> ├── input -> /media/hao/WangHao'Disk/bag_record  
> ├── output -> /media/hao/WangHao'Disk/bag_record  
> └── README.md

接着就可以开始解析了

```bash
python bag_parse.py
```
